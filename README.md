# givemebike

## Project setup
Before starting working on the project make sure that the following tools are installed:
* Git
* Latest LTS version of [NodeJS](https://nodejs.org/uk/) & [Yarn](https://yarnpkg.com/lang/en/)

## Development mode 
- Run `yarn serve` to start `http-server` on `http://localhost:8080`.
- Run `yarn start` to start the server.

## API usage
- We have our own API which we use in our application. 
   The API is located here `http://localhost:5000/givemebike`
   
## Build project
- Build the project

  ```bash
  $ npm run build or  $ yarn build
  ```
- `build/` - here you will find build artifacts


## Project structure
- `src/` - directory for code sources
    - `assets/` - directory for static assets
    - `components/` - directory for vue components
    - `constants/` - directory for application constants
    - `plugins/` - directory for plugins
    - `services/` - directory for API services
    - `store/` - directory for Vuex store
    - `interfaces/` - directory for interfaces
- `build/` - directory for built pages
- `server/` - directory where are the server files