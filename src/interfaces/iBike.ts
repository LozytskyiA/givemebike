export interface MyBike {
  _id?: number;
  name: string;
  type: string;
  price: string;
  available: boolean;
  time: string;
}
