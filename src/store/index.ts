import Vue from 'vue';
import Vuex from 'vuex';
import { MyBike } from '@/interfaces/iBike';
import moment from 'moment';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLoading: true,
    bikes: [],
    isBikeMoved: true,
  },
  mutations: {
    setLoading(state, payload: {is: boolean}) {
      state.isLoading = payload.is;
    },
    setBikes(state, payload: {bikes: []}) {
      state.bikes = payload.bikes;
    },
    setBikeMoved(state, payload: {is: boolean}) {
      state.isBikeMoved = payload.is;
    },
  },
  actions: {
    setLoading(context, payload) {
      context.commit('setLoading', payload);
    },
    setBikeMoved(context, payload) {
      context.commit('setBikeMoved', payload);
    },
  },
  getters: {
    availableBikes(state) {
      return state.bikes.filter((bike: MyBike) => bike.available);
    },
    rentedBikes(state) {
      return state.bikes.filter((bike: MyBike) => !bike.available);
    },
    rentedBikesTotalSum(state, { rentedBikes }) {
      return rentedBikes
        .reduce((previousValue: number, bike: MyBike) => (
          (moment().diff(moment(bike.time), 'hour') + 1) > 20
            ? Number(previousValue) + (moment().diff(moment(bike.time), 'hour') + 1)
              * (Number(bike.price) * 0.5)
            : Number(previousValue) + (moment().diff(moment(bike.time), 'hour') + 1)
              * Number(bike.price)), '');
    },
    rentedBikesCount(state, { availableBikes }) {
      return availableBikes.length;
    },
  },
  modules: {
  },
});
