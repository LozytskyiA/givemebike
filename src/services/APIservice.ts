import axios from 'axios';
import { MyBike } from '@/interfaces/iBike';


const bikeUrl = axios.create({
  baseURL: 'http://localhost:5000/',
});

const APIService = {
  async getBikes() {
    return bikeUrl.get('givemebike');
  },

  // getBike(id: string) {
  //   return bikeUrl.get(`givemebike/${id}`);
  // },

  async postBike(bike: MyBike) {
    return bikeUrl.post('givemebike', bike);
  },

  updateBike(id: number, available: { available: boolean; time?: string }) {
    return bikeUrl.put(`givemebike/${id}`, available);
  },

  deleteBike(id: number) {
    return bikeUrl.delete(`givemebike/${id}`);
  },
};

export default APIService;
