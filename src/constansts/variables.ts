export const bikeTypes = [
  'Custom', 'Street', 'Road', 'Mountain', 'Hybrid', 'Electric', 'Touring', 'Women’s',
];

export const inputNameRules = [(value: string) => !!value || 'Name is required'];
export const inputPriceRules = [
  (value: string) => !!value || 'Price is required',
  (value: string) => Number(value) || 'must be a number like 7 or 7.00'];
