const Bikes = require('../models/bikes');

exports.all = function (req, res) {
    Bikes.all(function (err, docs) {
        if(err) {
            console.log(err);
            return res.sendStatus(500)
        }
        res.status(200).send(docs)
    })
};

exports.findById = function (req, res) {
    Bikes.findById(req.params.id, function (err, doc) {
        if (err) {
            console.log(err);
            return res.sendStatus(500);
        }
        res.send(doc)
    })
};

exports.create = function (req, res) {
    const bike = {
      name: req.body.name,
      type: req.body.type,
      price: req.body.price,
      available: req.body.available,
      time: req.body.time,
    };

    Bikes.create(bike, function (err, result) {
        if(err) {
            console.log(err);
            return res.sendStatus(500)
        }
        res.send(bike)
    })
};

exports.update = function (req, res) {
    Bikes.update(req.params.id, {
      available: req.body.available,
      time: req.body.time,
    }, function (err, result) {
        if(err) {
            console.log(err);
            return res.sendStatus(500);
        }
        res.sendStatus(200)
    })
};

exports.delete = function (req, res) {
    Bikes.delete(req.params.id, function (err, result) {
        if(err) {
            console.log(err);
            return res.sendStatus(500);
        }
        res.sendStatus(200)
    })
};
