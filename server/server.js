const express = require('express');
const bodyParser = require('body-parser');
const db = require('./db');
const bikesController = require('./controllers/bikes');

const app = express();

app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.get('/', function (req, res) {
    res.send('Hello biker');
});

app.get('/givemebike', bikesController.all);

app.get('/givemebike/:id', bikesController.findById);

app.post('/givemebike', bikesController.create);

app.put('/givemebike/:id', bikesController.update);

app.delete('/givemebike/:id', bikesController.delete);


db.connect('mongodb+srv://gaarasandy:Vjqlytdybr2@cluster0-bpez6.mongodb.net/test?retryWrites=true&w=majority', function (err) {
    if (err) {
        return console.log(err)
    }
    app.listen(process.env.PORT || 5000, function () {
        console.log('Beri bike skoree')
    });
});
