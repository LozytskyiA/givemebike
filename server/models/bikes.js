const ObjectID = require('mongodb').ObjectID;
const db = require('../db');

exports.all = function (cb) {
    db.get().collection('givemebike').find().toArray(function (err, docs) {
        cb(err, docs)
    })
};

exports.findById = function (id, cb) {
    db.get().collection('givemebike').findOne({ _id: ObjectID(id) }, function (err, doc) {
        cb(err, doc);
    })
};

exports.create = function (bike, cb) {
    db.get().collection('givemebike').insertOne(bike, function (err, result) {
        cb(err, result);
    })
};

exports.update = function (id, newData, cb) {
    db.get().collection('givemebike').updateOne(
        { _id: ObjectID(id) },
        { $set: newData },
        { upsert: true },
        function (err, result) {
            cb(err, result)
        }
    )
};

exports.delete = function (id, cb) {
    db.get().collection('givemebike').deleteOne(
        { _id: ObjectID(id ) },
        function (err, result) {
            cb(err, result)
        }
    )
};
